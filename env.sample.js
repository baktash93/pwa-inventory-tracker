var config = {
    dbHost: 'mongodb://127.0.0.1/',
    dbName: 'pwa',
    port: 3001
};

module.exports = config;