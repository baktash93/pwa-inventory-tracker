var mongoose = require('mongoose');

var assetSchema = mongoose.Schema({
    serial: String,
    available: Number,
    description: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    isSynced: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Asset', assetSchema);