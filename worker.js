this.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open('v1').then(function (cache) {
            return cache.addAll([
                '/',
                '/stylesheets/style.css',
                '/npm/angular/angular.js',
                '/javascripts/ng-app.js',
                '/register-worker.js',
                '/worker.js',
                '/npm/bootstrap/dist/css/bootstrap.css'
            ]);
        })
    );
});

this.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                return response || fetch(event.request).then(function (response) {
                        return caches.open('v1').then(function (cache) {
                            // cache.put(event.request, response.clone());
                            return response;
                        })
                    });
            })
            .catch(function (e) {
                console.error("Service worker fetch error.", e);
            })
    );
});