var env;
var defaultEnv = {
    dbHost: 'mongodb://127.0.0.1/',
    dbName: 'pwa',
    port: 3000
};

(function init () {
    try {
        env = require('./../env');
    } catch (e) {
        env = defaultEnv;

    } finally {
        process.env.PORT = env.port;
        console.log("---------------------------------------------------");
        console.log("-----------------App configuration-----------------");
        console.log(env);
        console.log("---------------------------------------------------");

    }
})();

module.exports = env;