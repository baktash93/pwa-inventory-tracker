var mongoose = require('mongoose');
var config = require('./config');

module.exports = function (){
    connect();
};

function connect() {
    mongoose.connect(config.dbHost + config.dbName)
        .then(function () {
            console.log("CONNECTION TO MONGOOSE SUCCESSFULL.");
        }, function () {
            console.log("CONNECTION TO MONGOOSE FAILURE.");
        });
}