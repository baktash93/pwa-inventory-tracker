var express = require('express');
var path = require('path');

module.exports = function (app) {
    app.use('/npm', express.static(path.normalize(__dirname + '/../node_modules')));
}