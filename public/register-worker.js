if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/worker.js', {scope: "/"})
        .then(function (registrar) {
            if(registrar.installing) {
                console.log('=========================================');
                console.log('Service worker installing');
            } else if(registrar.waiting) {
                console.log('=========================================');
                console.log('Service worker installed');
            } else if(registrar.active) {
                console.log('=========================================');
                console.log('Service worker active');
            }
            console.log("-----------------------------------------");
        })
        .catch(function (error) {
            console.log("Registration failure: ", error);
        });
} else {
    alert('Your browser does not support Service Workers.')
}