angular.module('pwa', [])
    .controller('inventory', ['$scope', '$rootScope', '$http', '$timeout', 'AssetResourceProvider', 'OfflineStorageProvider', 'NetworkStateProvider', function ($scope, $http, $rootScope, $timeout, AssetResourceProvider, OfflineStorageProvider, NetworkStateProvider) {
        var assets = $scope.assets = [];
        var asset = $scope.asset = {};

        setTimeout(function () {
            AssetResourceProvider.all().then(function (assets) {
                console.log("fetched assets", assets);
                $scope.assets = assets;
            });
        }, 100);

        $scope.submitAsset = function () {
            AssetResourceProvider.create(angular.copy(asset))
                .then(function (_asset) {
                    $scope.assets.unshift(angular.copy(_asset));
                })
                .catch(function (error) {
                    console.log("Error creating resource", error);
                });
        };

        $scope.sync = function () {
            OfflineStorageProvider.sync()
                .then(function (syncCount) {
                    console.log('SYNC SUCCESSFUL!');
                    if (syncCount > 0) {
                        alert('SYNC SUCCESSFUL!');
                    } else {
                        alert('NO ASSETS IN INVENTORY TO SYNC!');
                    }
                    return AssetResourceProvider.all();
                })
                .then(function (assets) {
                    console.log("REFRESHING INVENTORY...");
                    $scope.assets = assets;
                })
                .catch(function (err) {
                    console.log('Failed to sync some resources.', err);
                });
        };

        $scope.$on('stateChange', function (event, state) {
            $scope.isOnline = state;
        })
    }])
    .service('NetworkStateProvider', function ($rootScope, $http) {
        var _context = this;
        this.isOnline = false;

        function _ping(context) {
            $http.get('/ping')
                .then(function () {
                    context.isOnline = true;
                    $rootScope.$broadcast('stateChange', context.isOnline)
                })
                .catch(function (err) {
                    console.error(err);
                    context.isOnline = false;
                    $rootScope.$broadcast('stateChange', context.isOnline)
                });
        }

        // initial ping
        setTimeout(_ping, 0, _context);
        ping = function ping() {
            setInterval(_ping, 3000, _context);
        }();
    })
    .service('AssetResourceProvider', ['$http', '$q', 'NetworkStateProvider', 'OfflineStorageProvider', function ($http, $q, NetworkStateProvider, OfflineStorageProvider) {
        var url = this.url = '/inventory';

        return {
            create: function (data) {
                if (NetworkStateProvider.isOnline) {
                    console.log('APP ONLINE: Sending request...');
                    data.isSynced = true;
                    return $http.post(url, data)
                        .then(function () {
                            return data;
                        });
                } else {
                    console.warn('APP OFFLINE: Deferring request...');
                    OfflineStorageProvider.store({
                        method: 'post',
                        url: url,
                        payload: data
                    });
                    return $q.resolve(data);
                }
            },
            all: function () {
                var pAlls = [],
                    assets = [];
                if (NetworkStateProvider.isOnline) {
                    pAlls.push($http.get(url));
                }
                pAlls.push(OfflineStorageProvider.getAll());
                return $q.all(pAlls).then(function (responses) {
                    var allAssets = responses.map(function (item){
                        return item.data;
                    });
                    for(var i = 0; i < allAssets.length; i++) {
                        assets = assets.concat(allAssets[i]);
                    }
                    return assets;
                })
            }
        };
    }])
    .service('OfflineStorageProvider', function ($q, $http, $window) {
        this.offlineStorage = $window.localStorage;
        var offlineStorage = this.offlineStorage;
        var _inventory = JSON.parse(offlineStorage.getItem("inventory")) || [];
        console.log("INVENTORY", _inventory);

        this.store = function (item) {
            _inventory.push(item);
            offlineStorage.setItem("inventory", JSON.stringify(_inventory));
        };

        this.getAll = function () {
            var _assets = _inventory.map(function (item) {
                return item.payload;
            });
            return $q(function (resolve, reject) {
                console.log('getAll called.');
                return resolve({data: _assets});
            });
        };

        this.sync = function () {
            var syncedItemsCounter = 0;
            return _inventory.reduce(function (promise, item) {
                return promise.then(function () {
                    syncedItemsCounter++;
                    _inventory[syncedItemsCounter - 1].isSynced = true;
                    return $http[item.method](item.url, item.payload);
                });
            }, $q.resolve())
                .then(function () {
                    _inventory = [];
                    offlineStorage.setItem("inventory", null);
                    return syncedItemsCounter;
                });
        };
    });