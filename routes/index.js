var express = require('express');
var path = require('path');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'PWA: Inventory Tracker' });
});

router.get('/ping', function (req, res) {
  res.sendStatus(200);
});

router.get('/worker.js', function (req, res) {
  res.sendFile(path.normalize(__dirname + '/../worker.js'));
});

module.exports = router;
