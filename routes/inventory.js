var express = require('express');
var router = express.Router();
var Asset = require('./../models/asset');


router.get('/', function (req, res) {
    Asset.find()
        .exec()
        .then(function (assets) {
            res.status(200)
                .send(assets);
        })
        .catch(function (err) {
            res.status(500)
                .send(err);
        });
});

router.post('/', function (req, res) {
    Asset.create(req.body)
        .then(function () {
            res.sendStatus(201);
        });
    res.send('ok');
});

module.exports = router;